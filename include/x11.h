/******************************************************************************
* x11.h: x11 related functions
 *****************************************************************************/
#ifndef X11_H
#define X11_H
#include "data.h"

extern int extended_formatting;
extern int x11_x_pos;
extern int x11_y_pos;
extern char *fontname;
extern char *fontsize;
extern char *color;

/* struct to hold a formatted string and its characteristics */
typedef struct {
    char *str;	/* pointer to the beginning of the string */
    int len;	/* length of the string */
    int bold;	/* use bold style */
    int italic;	/* use italic style */
    char *color;/* not implemented yet */
} formatted_t;

int setup_x11(void);
void x11_print_data(data_t *d);
void setup_font();

int check_x_pos(char *numeric);
int check_y_pos(char *numeric);

#endif /* End of X11_H */
