/******************************************************************************
* thread.h: functions for threaded implementation
 *****************************************************************************/
#ifndef THREAD_H
#define THREAD_H

void *monitor_thread(void *data);
void *display_thread(void *data);

#endif /* End of THREAD_H */
