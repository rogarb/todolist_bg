/******************************************************************************
* error.h: error handling
 *****************************************************************************/
#ifndef ERROR_H
#define ERROR_H
#include <stdio.h>
#include <time.h>

#ifdef LOG
/* logging infrastructure */
#define error(fmt,...) \
    do { \
	time_t ts = time(NULL);\
	struct tm *ttm = localtime(&ts);\
	fprintf(stderr, "%s: " fmt "\n", __func__, ##__VA_ARGS__);\
	if (log_started) {\
	    /* print a timestamp in the format DD/MM/YYYY-HH:MM:SS */\
	    fprintf(logstream, "[%02d/%02d/%02d-%02d:%02d:%02d] %s: " fmt "\n",\
		    ttm->tm_mday, ttm->tm_mon, ttm->tm_year+1990,\
		    ttm->tm_hour, ttm->tm_min, ttm->tm_sec,\
		    __func__, ##__VA_ARGS__);\
	    fflush(logstream);\
	}\
    } while (0)

void setup_logging(void);

extern FILE *logstream;
extern char *logfile;
extern int log_started;

#else /* LOG */
#define error(fmt, ...)	fprintf(stderr, "%s: " fmt "\n", __func__, ##__VA_ARGS__)
#endif /* LOG */

enum errcodes {
    ERR_IO = 1,	
    ERR_NULL,	/* NULL pointer error */
    ERR_X11,
    ERR_XFT,
};


#endif /* End of ERROR_H */
