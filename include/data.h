/******************************************************************************
* data.h: internal data representation and handling
 *****************************************************************************/
#ifndef DATA_H
#define DATA_H

#define MAX_LINES	20  /* max lines to print on screen */
#define MAX_LINESIZE   255  /* max number of characters per line */

struct _text_data {
    char lines[MAX_LINES][MAX_LINESIZE+1];
    int	nlines;
    int lock;
};

typedef struct _text_data data_t;

//extern data_t *data;

void setup_data(char *filename, int print_nlines);
void update_data(char *filename);
void data_to_screen(void);
int data_readable(void);
int fill_data_from_file(char *file, data_t *d);
void print_data(data_t *d);
int check_nlines(char *lines_arg);

#endif /* End of DATA_H */
