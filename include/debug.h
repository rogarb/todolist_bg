/******************************************************************************
 * debug.h: Debugging macros
 *****************************************************************************/
#ifndef DEBUG_H
#define DEBUG_H
#include "error.h"

#ifdef DEBUG
#define dbg(fmt, args...) \
    do {\
	printf("[DEBUG] %s: " fmt "\n", __func__, ##args);\
    } while (0)
#else
#define dbg(unused, ...)
#endif /* DEBUG */

#endif /* End of DEBUG_H */
