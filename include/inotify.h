/******************************************************************************
* inotify.h: inotify monitor helpers
 *****************************************************************************/
#ifndef INOTIFY_H
#define INOTIFY_H

void start_monitor(char *filename);

#ifdef TEST 
void __start_monitor(char *f, void (*cb)(char *f));
#endif

#endif /* End of INOTIFY_H */
