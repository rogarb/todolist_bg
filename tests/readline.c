/* test for readline function */
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "readfile.h"

int main(void)
{
    char *fname = "data.txt";   /* file used for test */

    FILE *fd = fopen(fname, "r");

    if (fd == NULL) {
	fprintf(stderr, "Error opening file \"%s\": %s\n", 
		fname, strerror(errno));
	return 1;
    }

    int linesizes[] = { 255, 10, 35, 0, 1 };
    int nsizes = sizeof(linesizes)/sizeof(int);

    for (int i = 0 ; i < nsizes ; i++) {
	int linecount = 0;
	int lsize = linesizes[i];
	printf("--------------------------------------\n");
	printf("Testing for linesize = %d\n", lsize);
	printf("--------------------------------------\n");

	char *buf = malloc(sizeof(char)* (lsize+1)); /* +1 for '\0' */
	int nread;

	while ((nread = readline(fd, buf, lsize+1)) > 0) {
	    linecount++;
	    printf("%2d: %s (nread = %d, strlen = %d)\n", 
		   linecount, buf, nread, strlen(buf));
	}
	free(buf);
	rewind(fd); /* reset file offset before new test */
    }
    
    fclose(fd);
    return 0;
}
