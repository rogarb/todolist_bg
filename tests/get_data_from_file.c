#include <stdio.h>

#include "data.h"

int main(void)
{
    data_t d;
    char *fname = "data.txt";

    if (fill_data_from_file(fname, &d) < 0)
	return 1;

    print_data(&d);

    return 0;
}

