#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>

#include "x11.h"
#include "data.h"

int main(void)
{
    data_t d = {
	.lines = { "AZERTY", "QSDFG", "WXCVB", "POIUY", "MLKJJ" },
	.nlines = 5 
    };

    if (setup_x11() < 0)
	return 1;

    printf("Printing data on root window, press ctrl-C to exit\n");
    for ( ; ; ) {
	x11_print_data(&d);
    }

    return 0;
}
