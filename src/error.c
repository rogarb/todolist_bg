#include <stdio.h>
#include <time.h>

#include "file.h"
#include "error.h"

#ifdef LOG
FILE *logstream = NULL;
char *logfile = "/home/romain/.local/share/todo/log";

int log_started = 0;

void setup_logging(void)
{
    time_t start_time = time(NULL);
    /* check if logfile exists and create it if necessary */
    if (!file_exists(logfile))
	create_file(logfile);

    /* open file stream */
    logstream = fopen(logfile, "a");
    if (logstream == NULL) {
	error("Error opening logfile");
    } else {
	fprintf(logstream, "-------------------------------------------\n");
	fprintf(logstream, "%s: starting logging at %s\n", 
		__func__, ctime(&start_time));
	fflush(logstream);
	log_started = 1;
    }
}

/* returns 1 of logging infrastructure is started, 0 elsewise */
int logging_started(void)
{
    return (log_started ? 1 : 0);
}
#endif /* LOG */
