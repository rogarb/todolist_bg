#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "data.h"
#include "thread.h"
#include "x11.h"
#include "error.h"

#ifdef LOG
#define LOGOPT "l:"
#else
#define LOGOPT
#endif /* LOG */
int usage(char *progname);

int main(int argc, char *argv[])
{
    char *filename = NULL;  /* full path to todofile */
    int daemonize = 0;	    /* daemonize flag */
    int opt;
    int printlines = -1;    /* number of lines to print on screen */

    pthread_t monitor, display;
    
    /* X11 has to be setup before options parsing since we need to know
     * the charateristics of the display */
    if (setup_x11() < 0)
	return 1;
    while ((opt = getopt(argc, argv, "hdt:x:y:f:s:c:en:" LOGOPT)) != -1) {
	switch (opt) {
	    case 'h':
		usage(argv[0]);
		break;
	    case 'd':
		daemonize = 1;
		break;
	    case 't':
		filename = optarg;
		break;
	    case 'x':
		x11_x_pos = check_x_pos(optarg);
		break;
	    case 'y':
		x11_y_pos = check_y_pos(optarg);
		break;
	    case 'f':
		fontname = optarg;
		break;
	    case 's':
		fontsize = optarg;
		break;
	    case 'c':
		color = optarg;
		break;
	    case 'e':
		extended_formatting = 1;
		break;
	    case 'n':
		printlines = check_nlines(optarg);
		break;
#ifdef LOG
	    case 'l':
		logfile = optarg;
		break;
#endif
	    default:
		usage(argv[0]);
		break;
	}
    }

#ifdef LOG
    setup_logging();
#endif

    if (filename == NULL) {
	error("Option -t must be set");
	exit(EXIT_FAILURE);
    }

    setup_data(filename, printlines);

    /* setup font according to options */
    setup_font();

    if (daemonize)
        daemon(0, 0);   /* the process has to be daemonized before starting 
			 * the threads */

    pthread_create(&monitor, NULL, &monitor_thread, filename);
    pthread_create(&display, NULL, &display_thread, NULL);

//    while (pthread_tryjoin_np(monitor, NULL) != 0 && pthread_tryjoin_np(display, NULL) != 0);
    pthread_join(monitor, NULL);

    return 0;
}

int usage(char *progname)
{
    printf("Usage: %s [opts] -t todofile  Display the content of todofile on the root window\n", progname);
    printf("       %s -h                  Show this help\n", progname);
    printf("\n");
    printf("where [opts] can be any of the following:\n");
    printf("\t-x pos: specify the x position of the upper left part of the text\n");
    printf("\t-y pos: specify the y position of the upper left part of the text\n");
    printf("\t-d: run in background\n");
    printf("\t-f fontname: change the font\n");
    printf("\t-s fontsize: change the font size\n");
    printf("\t-c fontcolor: change the font color\n");
    printf("\t-e: extended text format, print formatted text\n");
    printf("\t-n num: print maximum num lines (default: %d)\n", MAX_LINES);
#ifdef LOG
    printf("\t-l logfile: specify the file to use for logging\n");
#endif /* LOG */
    printf("\n");
    printf("Formatted text:\n");
    printf("\t*text* prints bold text\n");
    printf("\t/text/ or _text_ prints italic text\n");

    exit(EXIT_FAILURE);
}
