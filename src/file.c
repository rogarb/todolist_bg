#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include "file.h"
#include "error.h"
#include "debug.h"

/* check that the file exists, we expect a full path !!
 * return 1 if file exists, 0 elsewise */
int file_exists(char *filename)
{
    struct stat unused;

    if (stat(filename, &unused) < 0)
	return 0;

    return 1;
}

/* checks if the given path is a directory, we expect a full path
 * return 1 if path is a directory, 0 elsewise */
int path_is_dir(char *path)
{
    struct stat s;

    /* return 0 if there is an error accessing the stat structure */
    if (stat(path, &s) < 0)
	return 0;

    if (S_ISDIR(s.st_mode)) /*  we use the POSIX macro to test*/
	return 1;
    return 0;
}

/* return the mode of a file given a file descriptor */
int get_mode(int fd)
{
    struct stat s;

    if (fstat(fd, &s) < 0)
	error("Unable to get fd stats: %s\n", strerror(errno));

    return (s.st_mode & 07777);
}

/* recursive function to create all the necessary subdirs */
void create_dir(char *path)
{
    char *copy = strdup(path);
    char *last_pos = strrchr(copy, '/');

    if (last_pos == copy || last_pos == NULL || path_is_dir(copy)) {
	free(copy);
	return;
    }

    if (copy != NULL) {
	*last_pos = '\0';
	create_dir(copy);
	mkdir(path, 0700);
    }
    free(copy);
}

void create_file(char *fullpath)
{
    char *path = strdup(fullpath);
    char *last_pos = strrchr(path, '/');

    if (last_pos == NULL) {
	error("%s is not a full path\n", fullpath);
    } else {
	*last_pos = '\0';
	create_dir(path);
	free(path);
    }

    FILE *file = fopen(fullpath, "w");
    if (file == NULL)
	error("Unable to create %s: %s\n", fullpath, strerror(errno));

    fclose(file);
}

/* check if file is empty: returns 1 for an empty file, 0 elsewise */
int file_is_empty(char *filename)
{
    struct stat s;
    int ret = 0;

    if (stat(filename, &s) == -1) {
	printf("Error: unable to get stats for file %s: %s\n",
		filename, strerror(errno));
	return 0;
    }

    if (s.st_size == 0)
	ret = 1;

    dbg("%s is%sempty (st_size = %ld)", filename, (ret ? " " : " not "), s.st_size);
    return ret;
}
