/* inotify.c - iontify helpers */
#include <sys/inotify.h>
#include <unistd.h>
#include <stdio.h>

#include "data.h"
#include "error.h"
#include "debug.h"

#ifdef DEBUG
void __print_event_name(struct inotify_event *p);
#endif

void __start_monitor(char *filename, void (*callback)(char *))
{
    unsigned char buf[1024] = {0};

    int fd = inotify_init();
    int mask = IN_ALL_EVENTS;
    int wd;

    if ((wd = inotify_add_watch(fd, filename, mask)) < 0) {
	error("Unable to add inotify watch");
	return;
    }

    while (1) {
	read(fd, buf, 1024);
//	printf("Read %d bytes\n", n);
	struct inotify_event *p = (struct inotify_event *) buf;
#ifdef DEBUG
	__print_event_name(p);
#endif
	if (p->mask == IN_MODIFY) {
	    callback(filename);
	} else if (p->mask == IN_ATTRIB) {
	/* IN_ATTRIB allows detection of edition by programs like vim which
	 * copy the original file to a .swp file and overwrite the original
	 * file when saving
	 * we close and open the watch descriptor again so we keep following
	 * its changes */
	    inotify_rm_watch(fd, wd);
	    if ((wd = inotify_add_watch(fd, filename, mask)) < 0) {
		error("Unable to add inotify watch");
		return;
	    }
	    callback(filename);
	}
    }
}

void start_monitor(char *filename)
{
    __start_monitor(filename, update_data);
}

#ifdef DEBUG
void __print_event_name(struct inotify_event *p)
{
    char *str = NULL;
    switch (p->mask) {
	case IN_ACCESS:
	    str = "File was accessed.";
	    break;
	case IN_MODIFY:
	    str = "File was modified.";
	    break;
	case IN_ATTRIB:
	    str = "Metadata changed.";
	    break;
	case IN_CLOSE_WRITE:
	    str = "Writtable file was closed.";
	    break;
	case IN_CLOSE_NOWRITE:
	    str = "Unwrittable file closed.";
	    break;
	case IN_OPEN:
	    str = "File was opened.";
	    break;
	case IN_MOVED_FROM:
	    str = "File was moved from X.";
	    break;
	case IN_MOVED_TO:
	    str = "File was moved to Y.";
	    break;
	case IN_CREATE:
	    str = "Subfile was created.";
	    break;
	case IN_DELETE:
	    str = "Subfile was deleted.";
	    break;
	case IN_DELETE_SELF:
	    str = "Self was deleted.";
	    break;
	case IN_MOVE_SELF:
	    str = "Self was moved.";
	    break;
	case IN_CLOSE:
	    str = "Close.";
	    break;
	case IN_MOVE:
	    str = "Moves.";
	    break;
	case IN_IGNORED:
	    str = "Ignored. (watch is removed)";
	    break;
	default:
	    str = "Undefined notification";
	    break;
    }
    error("%s", str);
}
#endif /* DEBUG */
