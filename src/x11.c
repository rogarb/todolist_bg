#include <X11/Xlib.h>
#include <X11/Xft/Xft.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <errno.h>

#include "x11.h"
#include "error.h"
#include "debug.h"

#define toggle(flag)	(flag = (flag ? 0 : 1))

#define MAX_FMT	8   /* max number of characters that can be used to print
		     * the format string */

/* X11 vars */
Display *display;
int scr_num;
Window root;
Visual *visual;
Colormap cmap;
XWindowAttributes root_attr;
GC gc_root;
XFontSet fontset;
XftFont *font_reg;  /* Regular style font */
XftFont *font_it;   /* Italic style font */
XftFont *font_bd;   /* Bold style font */
XftFont *font_bdit; /* Bold Italic style font */
XftDraw *draw;
XftColor xftcolor;
int blackColor, whiteColor;

/* default values for configurable parameters */
const int def_x_pos = 0; /* default values for x,y initial position */
const int def_y_pos = 20;
const int def_fontsize = 14;

/* configurable parameters */
int extended_formatting = 0;
int x11_x_pos = def_x_pos;
int x11_y_pos = def_y_pos;
int interline = 30;	/* interline space in pixels */
char *fontsize = "14";	/* store fontsize as a string for easier handling */
char *fontname = "Latin Modern Roman";
char *color = "#FFFFFF";    /* white */
char *format = "%2d. ";   /* string formatter used for line numbering */

/* private functions */
int x11_strlen(char *s, int bold, int italic);
static int x11_fmtlen(formatted_t f);
static int x11_fmtheight(formatted_t f);
int x11_putchar(char c, int x, int y, int bold, int italic);
int x11_puts(char *s, int x, int y, int bold, int italic);
int x11_putformatted(formatted_t str, int x, int y);
int x11_draw_formatted_line(char *formatted, int x, int y);
int x11_draw_raw_line(char *str, int x, int y);
XftFont *select_font(int bold, int italic);
static int parse_str(char **str, formatted_t *f);
static int split_formatted(formatted_t in, int width, formatted_t **out);
static void strip_fmt(formatted_t *str);
#ifdef DEBUG
void __print_event(int type);
#endif

/* strips the beginning white spaces in formatted string */
static void strip_fmt(formatted_t *str)
{
    if (str == NULL)
	return;
    for (char *p = str->str ; p != NULL && *p == ' ' ; p++) {
	str->str++;
	str->len--;
    }
}

/* fills a formatted string out of a raw string
 * this function is to be called multiple times until str is set to NULL
 * meaning that the parsing is done
 * returns 0 when done (str is NULL), 1 otherwise */
static int parse_str(char **str, formatted_t *f)
{
    int ret = 0;
    int escape = 0;
    char c = '\0';
    char *start = *str;
    char *stop = *str;

    if (str == NULL || f == NULL)
	return ret;

    /* we separate the formatted string in substrings that weprint with the
     * adequate style */
    ret = 1;
    while ((c = *stop) != '\0') {
	if (strchr("*_/\\", c) != NULL) {
	    if (escape == 1) {
		escape = 0;
	    } else {
		if (c != '\\') {
		    if (start != stop) {
			break;
		    }
		    switch (c) {
			case '\\':
			    escape = 1;
			    break;
			case '*':
			    toggle(f->bold);
			    break;
			case '/':
			case '_':   /* treat _ as in markdown */
			    toggle(f->italic);
			    break;
			default:
			    break;
		    }
		    start++;
		} else {
		    escape = 1;
		}
	    }
	}
	stop++;
    }
    f->str = start;
    f->len = stop - start;
    *str = stop;
    if (start == stop) {
	*str = NULL;
	ret = 0;
    }
    return ret;
}

/* return the correct font according to characteristics */
XftFont *select_font(int bold, int italic)
{
    XftFont *font = NULL;
    if (bold) {
	if (italic)
	    font = font_bdit;
	else
	    font = font_bd;
    } else if (italic) {
	font = font_it;
    } else {
	font = font_reg;
    }
    return font;
}

/*  returns the height of the printed formatted string on screen */
static int x11_fmtheight(formatted_t f)
{
    XGlyphInfo ext;
    XftFont *ft = select_font(f.bold, f.italic);

    XftTextExtentsUtf8(display, ft, (const FcChar8*) f.str, f.len, &ext);
    return ext.height;
}

/*  returns the width of the printed formatted string on screen */
static int x11_fmtlen(formatted_t f)
{
    XGlyphInfo ext;
    XftFont *ft = select_font(f.bold, f.italic);

    XftTextExtentsUtf8(display, ft, (const FcChar8*) f.str, f.len, &ext);
    return ext.xOff;
}

/*  returns the width of the printed string on screen */
int x11_strlen(char *s, int bold, int italic)
{
    XGlyphInfo ext;
    XftFont *ft = select_font(bold, italic);

    XftTextExtentsUtf8(display, ft, (const FcChar8*) s, strlen(s), &ext);
    return ext.xOff;
}

/* draw a character at a specific position using a specific font and style 
 * return the width of drawn character */
int x11_putchar(char c, int x, int y, int bold, int italic)
{
    XftFont *font;
    XGlyphInfo extents;
    int drawn_pixels_width;

    /* Prepare the fontname string */ 
    if (bold) {
	if (italic)
	    font = font_bdit;
	else
	    font = font_bd;
    } else if (italic) {
	font = font_it;
    } else {
	font = font_reg;
    }

    XftTextExtentsUtf8(display, font, (const FcChar8*) &c, 1, &extents);
    drawn_pixels_width = extents.xOff;
    XftDrawStringUtf8(draw, &xftcolor, font, x, y, (const FcChar8*) &c, 1);

    return drawn_pixels_width;
}

/* draw a string at a specific position using a specific font and style 
 * return the width of drawn string */
int x11_puts(char *s, int x, int y, int bold, int italic)
{
    XftFont *font;
    XGlyphInfo extents;
    int drawn_pixels_width;
    int len = strlen(s);

    /* Prepare the fontname string */ 
    if (bold) {
	if (italic)
	    font = font_bdit;
	else
	    font = font_bd;
    } else if (italic) {
	font = font_it;
    } else {
	font = font_reg;
    }

    XftTextExtentsUtf8(display, font, (const FcChar8*) s, len, &extents);
    drawn_pixels_width = extents.xOff;
    XftDrawStringUtf8(draw, &xftcolor, font, x, y, (const FcChar8*) s, len);

    return drawn_pixels_width;
}

/* split the input formatted string into a malloc()'ed array of formatted 
 * strings fitting in width.
 * string is splitted according to the width parameter in pixels
 * returns the length of the array, -1 if unallocated */
static int split_formatted(formatted_t in, int width, formatted_t **out)
{
    int written_len = 0;
    int count = 0;
    char *p = NULL;
    char *end;
    formatted_t dup;
    formatted_t *array = NULL;
    memcpy(&dup, &in, sizeof(in));
    end = dup.str + dup.len;
    dbg("Splitting for width = %d(given array width = %d", width, x11_fmtlen(in));
    while (dup.len > 0) {
	/* find the longest string with space boundaries that fits on the
	 * screen  */
	p = end;
	if (x11_fmtlen(dup) > width) {
	    while (p != dup.str) {
		if (*p == ' ') {
		    dup.len = p - dup.str + 1;
		    if (x11_fmtlen(dup) < width) {
			break;
		    }
		}
		p--;
	    }
	}
	/* we found a substring, we copy it in a malloc()'ed array */
	array = (formatted_t *) realloc(array, (count+1)*sizeof(formatted_t));
	if (array == NULL)
	    return -1;
	memcpy(array+count, &dup, sizeof(dup));
	count++;
	dup.str += dup.len;
	written_len += dup.len;
	dup.len = in.len - written_len;
    }
    *out = array;
    return count;
}

/* draw a formatted string at a specific position
 * return the width of drawn string */
int x11_putformatted(formatted_t s, int x, int y)
{
    XftFont *font;
    XGlyphInfo extents;
    int drawn_pixels_width;

    /* Select the right font */
    if (s.bold) {
	if (s.italic)
	    font = font_bdit;
	else
	    font = font_bd;
    } else if (s.italic) {
	font = font_it;
    } else {
	font = font_reg;
    }

    XftTextExtentsUtf8(display, font, (const FcChar8*) s.str, s.len, &extents);
    drawn_pixels_width = extents.xOff;
    XftDrawStringUtf8(draw, &xftcolor, font, x, y,
	    (const FcChar8*) s.str, s.len);

    return drawn_pixels_width;
}

/* draw a formatted string on the root screen at position x, y */
int x11_draw_formatted_line(char *formatted, int x, int y)
{
    dbg("starting @(%d, %d)", x, y);
    int count = 0;
    int cur_x = x;
    int cur_y = y;
    int max_height = 0;
    formatted_t fmt_str = {
	.str = NULL,
	.len = 0,
	.bold = 0,
	.italic = 0,
	.color = NULL,
    };
    formatted_t *fmt_array = NULL;
    char *p = formatted;

    /* get the max height of all the strings */
    while (parse_str(&p, &fmt_str)) {
	int cur_height = x11_fmtheight(fmt_str);
	max_height = (max_height > cur_height ? max_height : cur_height);
    }

    /* print everything */
    p = formatted;
    while (parse_str(&p, &fmt_str)) {
	count = split_formatted(fmt_str, root_attr.width - cur_x, &fmt_array);
	for (int i = 0 ; i < count ; i++) {
	    if (cur_x + x11_fmtlen(fmt_array[i]) > root_attr.width) {
		cur_x = x;
		cur_y += max_height;
		/* strip beginning spaces when starting a newline */
		strip_fmt(fmt_array+i);
	    }
	    cur_x += x11_putformatted(fmt_array[i], cur_x, cur_y);
	}
	free(fmt_array);
    }
    /* flush the requests */
    XFlush(display);
    return cur_y - y;
}

/* draw a raw string on the root screen at position x, y 
 * returns the extra height if more than one line is printed, 0 otherwise */
int x11_draw_raw_line(char *str, int x, int y)
{
    int y_offset = 0;
    int y_cur = y;
    int x_cur = x;
    int line_width = x11_strlen(str, 0, 0);
    int max_width = root_attr.width - x;
    char *dup = strdup(str);
    char *print = dup;
    char *p = NULL;
    XGlyphInfo extents;
    XftTextExtentsUtf8(display, font_reg, (const FcChar8*) str,
	    strlen(str), &extents);
//     dbg("width = %d, height = %d, x = %d, y = %d, xOff = %d, yOff = %d",
// 	extents.width, extents.height, extents.x, extents.y,
// 	extents.xOff, extents.yOff);
//     int max_char = max_width/font_reg->max_advance_width;
//     dbg("max_width = %d, line_width = %d, max_advance_width = %d\n",
// 	    max_width, line_width, font_reg->max_advance_width);
    /* check if the line fits on screen */
    while ((line_width = x11_strlen(print, 0, 0)) > max_width) {
	/* find the longest string with space boundaries that fits on the
	 * screen  */
	p = strchr(print, '\0');
	while (p != print) {
	    if (*p == ' ') {
		*p = '\0';
		if (x11_strlen(print, 0, 0) >= max_width) {
		    *p = ' ';
		} else {
		    break;
		}
	    }
	    p--;
	}
	dbg("strlen(print) = %d, x11_strlen(print) = %d", strlen(print),
		x11_strlen(print, 0, 0));
	x11_puts(print, x_cur, y_cur, 0, 0);
	y_cur += extents.height;
	print = p+1;
	y_offset += extents.height;
    } 
    /* print the last part of the string (which is the full string if
     * line_width < max_width), so we don't update y_offset here */
    x11_puts(print, x_cur, y_cur, 0, 0);
    /* flush the requests */
    XFlush(display);
    free(dup);
    return y_offset;
}

/* checks that the value represented in the string fits the constraints
 * returns the value as int if it is valid, -1 elsewise */
int check_x_pos(char *string)
{
    int x = atoi(string);

    if (x < 0)
	error("Invalid negative value for -x option: %d, using default", x);

    if (x > root_attr.width) {
	error("Value above root window width: %d > %d, using default", x,
	       root_attr.width);
	x = -1;
    }
    return x;
}

/* see check_x_pos() */
int check_y_pos(char *string)
{
    int y = atoi(string);

    if (y < 0)
	error("Invalid negative value for -y option: %d, using default", y);

    if (y > root_attr.height) {
	error("Value above root window height: %d > %d, using default", y,
	       root_attr.height);
	y = -1;
    }
    return y;
}

/* this needs to be called *before* options parsing */
int setup_x11(void)
{
    /* this is important for working with UTF-8 */
    setlocale(LC_ALL, "");
    long int event_mask = ExposureMask 
			| EnterWindowMask 
			| VisibilityChangeMask
			| StructureNotifyMask
			| FocusChangeMask;

    /* open the display */
    display = XOpenDisplay(NULL);
    
    if (display == NULL) {
	error("Unable to connect to display");
	return -ERR_X11;
    }

    blackColor = BlackPixel(display, DefaultScreen(display));
    whiteColor = WhitePixel(display, DefaultScreen(display));

    scr_num = DefaultScreen(display);
    root = RootWindow(display, scr_num);
    visual = DefaultVisual(display, scr_num);
    cmap = DefaultColormap(display, scr_num);

    if (XGetWindowAttributes(display, root, &root_attr) == 0) {
	error("Error getting root window attributes, exiting");
	return -ERR_X11;
    }

    /* associate events */
#ifdef DEBUG
/* select all possible masks except forbidden ones for root screen (which give
 *  an access to private ressource error) */
//    event_mask = ~(~0L << 24) & ~(1<<2) & ~(1<<20);
#endif
    XSelectInput(display, root, event_mask);

    XSetWindowBackground(display, root, blackColor);
#ifdef DEBUG
    error("Display background set");
#endif

    return 0;
}

/* this needs to be called *after* options parsing */
void setup_font(void)
{
    const char *size_opt = ":size=";
    char *style_bdit = ":style=Bold Italic";
    char *style_bd = ":style=Bold";
    char *style_it = ":style=Italic";
    char *style_reg = ":style=Regular";
    char *ft;
    int len;
    
    /* Prepare the fontname string buffer */ 
    len = strlen(fontname)
	+ strlen(fontsize)
	+ strlen(size_opt)
	+ strlen(style_bdit); /* it is the longest string of the style_* */

    ft = malloc((len+1)*sizeof(char));
    if (ft == NULL) {
	error("Unable to allocate memory: %s", strerror(errno));
	exit(1);
    }

    /* Open every font style */
    strcpy(ft, fontname);
    strcat(ft, size_opt);
    strcat(ft, fontsize);
    int style_pos = strlen(ft);

    /* Open Regular style */
    strcpy(ft+style_pos, style_reg);
    if ((font_reg = XftFontOpenName(display, scr_num, ft)) == NULL) {
	error("Unable to open font \"%s\"", ft);
	exit(1);
    }
#ifdef DEBUG
    error("Opened font \"%s\"", ft);
#endif

    /* Open Bold style */
    strcpy(ft+style_pos, style_bd);
    if ((font_bd = XftFontOpenName(display, scr_num, ft)) == NULL) {
	error("Unable to open font \"%s\"", ft);
	exit(1);
    }
#ifdef DEBUG
    error("Opened font \"%s\"", ft);
#endif
    
    /* Open Italic style */
    strcpy(ft+style_pos, style_it);
    if ((font_it = XftFontOpenName(display, scr_num, ft)) == NULL) {
	error("Unable to open font \"%s\"", ft);
	exit(1);
    }
#ifdef DEBUG
    error("Opened font \"%s\"", ft);
#endif

    /* Open Bold Italic style */
    strcpy(ft+style_pos, style_bdit);
    if ((font_bdit = XftFontOpenName(display, scr_num, ft)) == NULL) {
	error("Unable to open font \"%s\"", ft);
	exit(1);
    }
#ifdef DEBUG
    error("Opened font \"%s\"", ft);
#endif

    if (XftColorAllocName(display, visual, cmap, color, &xftcolor) == 0) {
        error("Unable to allocate XftColor: %s", strerror(errno));
	exit(1);
    }
#ifdef DEBUG
    error("Allocated XftColor %s", color);
#endif
    
    if ((draw = XftDrawCreate(display, root, visual, cmap)) == NULL) {
	error("Unable to create XftDraw: %s", strerror(errno));
	exit(1);
    }
#ifdef DEBUG
    error("Created XftDraw");
#endif
}

#ifdef DEBUG
void __print_event(int type);
#endif 

void x11_print_data(data_t *d)
{
    int x = (x11_x_pos < 0 ? def_x_pos : x11_x_pos);
    int y = (x11_y_pos < 0 ? def_y_pos : x11_y_pos);
    XEvent e;
    XNextEvent(display, &e);
#ifdef DEBUG
/* debug: Print received event on terminal */
    __print_event(e.type);
#endif /* DEBUG */
    /* added ClientMessage to attempt fixing bug: not displaying at startup */
    if (e.type == Expose || e.type == ClientMessage) {
	for (int i = 0 ; i < d->nlines ; i++) {
	    char line_pre[MAX_FMT] = { '\0' };
	    int line_x = x;
	    if (format) {
		/* i starts at 0 and we want the first line number to be 1 so
		 * we print i+1 for the line number */
		snprintf(line_pre, MAX_FMT, format, i+1);
		x11_draw_raw_line(line_pre, line_x, y + i * interline);
		line_x += x11_strlen(line_pre, 0, 0);
	    }
	    if (extended_formatting)
		y += x11_draw_formatted_line(d->lines[i], line_x, y + i * interline);
	    else
		y += x11_draw_raw_line(d->lines[i], line_x, y + i * interline);
	}
    }
}

#ifdef DEBUG
void __print_event(int type)
{
    char *str = NULL;

    switch (type) {
	case KeyPress:
	    str = "KeyPress";
	    break;
	case KeyRelease:
	    str = "KeyRelease";
	    break;
	case ButtonPress:
	    str = "ButtonPress";
	    break;
	case ButtonRelease:
	    str = "ButtonRelease";
	    break;
	case MotionNotify:
	    str = "MotionNotify";
	    break;
	case EnterNotify:
	    str = "EnterNotify";
	    break;
	case LeaveNotify:
	    str = "LeaveNotify";
	    break;
	case FocusIn:
	    str = "FocusIn";
	    break;
	case FocusOut:
	    str = "FocusOut";
	    break;
	case KeymapNotify:
	    str = "KeymapNotify";
	    break;
	case Expose:
	    str = "Expose";
	    break;
	case GraphicsExpose:
	    str = "GraphicsExpose";
	    break;
	case NoExpose:
	    str = "NoExpose";
	    break;
	case VisibilityNotify:
	    str = "VisibilityNotify";
	    break;
	case CreateNotify:
	    str = "CreateNotify";
	    break;
	case DestroyNotify:
	    str = "DestroyNotify";
	    break;
	case UnmapNotify:
	    str = "UnmapNotify";
	    break;
	case MapNotify:
	    str = "MapNotify";
	    break;
	case MapRequest:
	    str = "MapRequest";
	    break;
	case ReparentNotify:
	    str = "ReparentNotify";
	    break;
	case ConfigureNotify:
	    str = "ConfigureNotify";
	    break;
	case ConfigureRequest:
	    str = "ConfigureRequest";
	    break;
	case GravityNotify:
	    str = "GravityNotify";
	    break;
	case ResizeRequest:
	    str = "ResizeRequest";
	    break;
	case CirculateNotify:
	    str = "CirculateNotify";
	    break;
	case CirculateRequest:
	    str = "CirculateRequest";
	    break;
	case PropertyNotify:
	    str = "PropertyNotify";
	    break;
	case SelectionClear:
	    str = "SelectionClear";
	    break;
	case SelectionRequest:
	    str = "SelectionRequest";
	    break;
	case SelectionNotify:
	    str = "SelectionNotify";
	    break;
	case ColormapNotify:
	    str = "ColormapNotify";
	    break;
	case ClientMessage:
	    str = "ClientMessage";
	    break;
	case MappingNotify:
	    str = "MappingNotify";
	    break;
	case GenericEvent:
	    str = "GenericEvent";
	    break;
	default:
	    str = "Unknown Event";
	    break;
    }
    error("DEBUG: Received event: %s", str);
}
#endif /* DEBUG */
