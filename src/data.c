#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <bsd/string.h>

#include "data.h"
#include "error.h"
#include "x11.h"

data_t data;
int maxlines = MAX_LINES;

/* private functions not declared in header */
int readline(FILE *f, char *buf, int buflen);

void data_to_screen(void)
{
    while (1) {
	if (data.lock == 0)
            x11_print_data(&data);
    }
}

void setup_data(char *filename, int printlines)
{
    memset(&data, 0, sizeof(data));

    if (printlines > 0)
	maxlines = printlines;
    
    if (fill_data_from_file(filename, &data) < 0)
	exit(1);
}

void update_data(char *filename)
{
    data.lock = 1;
    if (fill_data_from_file(filename, &data) < 0)
	exit(1);
    data.lock = 0;
}

int data_readable(void)
{
    return (data.lock = 0 ? 1 : 0);
}

int fill_data_from_file(char *file, data_t *d)
{
    if (d == NULL) {
	error("Pointer can't be NULL");
	return -ERR_NULL;
    }

    FILE *f = fopen(file, "r");
    
    if (f == NULL) {
	error("Unable to open file");
	return -ERR_IO;
    }

    memset(d, 0, sizeof(*d));

    while (readline(f, d->lines[d->nlines], MAX_LINESIZE+1) > 0 
	    && d->nlines < maxlines) {
       d->nlines++;
    }
    
    fclose(f);
    return 0;
}

void print_data(data_t *d)
{
    if (d == NULL) {
	error("Pointer can't be NULL");
	return;
    }

    printf("data_t content: %d lines\n", d->nlines);
    for (int i = 0 ; i < d->nlines ; i++) {
	printf("Line %d: %s\n", i+1, d->lines[i]);
    }
}

int readline(FILE *f, char *buf, int buflen) 
{

    int nread = 0;	    /* number of bytes read by read() */
    char *nl_pos = NULL;    /* position of '\n' char in tmp_buf */
    int lline = 0;	    /* long line flag */

    /* account for the extra '\n' which will
     * be stripped and '\0' as we work with fgets() */
    int tmp_buf_len = buflen+2;
    char *tmp_buf = malloc(sizeof(char) * (tmp_buf_len));

    if (tmp_buf == NULL) {
	error("Unable to allocate buffer");
	return nread;
    }

    /* erase memory before using */
    memset(tmp_buf, 0, tmp_buf_len);

    while (fgets(tmp_buf, tmp_buf_len, f) != NULL) {
/* debug */
// 	printf("tmp_buf:\n");
// 	int count = 0;
// 	for (int i = 0 ; i < tmp_buf_len ; i++) {
// 	    printf("0x%02X ", *(tmp_buf+i));
// 	    if (count++ > 80) {
// 		putchar('\n');
// 		count = 0;
// 	    }
// 	}
// 	putchar('\n');
// 	printf("tmp_buf[strlen(tmp_buff)-1] = 0x%02X:\n", tmp_buf[strlen(tmp_buf)-1]);
/* end debug */

	/* ignore long lines which don't fit in the buffer */
	if (tmp_buf[strlen(tmp_buf)-1] == '\n') {
	    if (lline)
		lline = 0;
	    else
		break;
	}
	else 
	    lline = 1;
	tmp_buf[0] = '\0'; /* empty string */
    }

    /* don't process if the buffer is empty */
    if ((nread = strlen(tmp_buf)) != 0) {
	if ((nl_pos = strchr(tmp_buf, '\n')) != NULL)
	    *nl_pos = '\0'; /* strip trailing newline */
	nread = strlcpy(buf, tmp_buf, buflen); /* from libbsd */
    }
    free(tmp_buf);
    /* return the number of characters copied from the file as we use this 
     * value to detect EOF */
    return nread;
}

/* check the nlines argument and return an acceptable value */
int check_nlines(char *lines_arg)
{
    int ret = MAX_LINES;
    int arg = atoi(lines_arg);

    if (arg > 0 && arg < MAX_LINES)
	ret = arg;
    else
	error("Wrong value for -n argument: %d\n", arg);

    return ret;
}
