#include <stdio.h>

#include "inotify.h"
#include "data.h"

void *display_thread(void *d)
{
    data_to_screen();
    return NULL;
}

void *monitor_thread(void *d) 
{
    char *fname = (char *)d;

    printf("Starting monitor thread\n");
    start_monitor(fname);

    return NULL;
}
