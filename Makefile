CC=/usr/bin/gcc
CFLAGS=-O2 -Wall -pedantic -Iinclude -pthread -DLOG\
       $(shell pkg-config --cflags freetype2 x11 libbsd xft)
INC=
DBG=-g -DDEBUG
LDFLAGS=$(shell pkg-config --libs freetype2 x11 libbsd xft)
SRC=$(wildcard src/*.c)
HEADERS=$(wildcard include/*.h)
EXE=todolist_bg
OBJ=$(subst .c,.o,$(SRC))
OBJ_WITHOUT_MAIN=$(subst src/main.o,,$(OBJ))
TEST_SRC=$(wildcard tests/*.c)
TEST_EXE=$(subst .c,,$(TEST_SRC))
DESTDIR=~/bin

#$(EXE): $(OBJ)
#	${CC} $(CFLAGS) $(LDFLAGS) -o $(EXE) $(OBJ)
$(EXE):
	${CC} $(SRC) $(shell pkg-config --cflags --libs x11 xft) -Iinclude -pthread -lbsd -o $(EXE)

.PHONY: debug
debug: CFLAGS+=$(DBG)
debug: $(OBJ)
	${CC} $(CFLAGS) $(LDFLAGS) -o $(EXE)-dbg $(OBJ)

.PHONY: all
all: $(EXE) build-tests

.PHONY: install
install: $(EXE)
	cp -f $(EXE) $(DESTDIR)

.PHONY: clean
clean:
	rm -f $(EXE) $(OBJ) $(TEST_OBJ) $(TEST_EXE) $(EXE)-dbg

##########################
# Testing infrastructure #
##########################

.PHONY: run-tests
run-tests: build-tests
	for TEST in $(subst tests/,,$(TEST_EXE)) ; do \
	    echo "Running $$TEST"; \
	    (cd tests && ./$$TEST); \
	done;

tests/%: tests/%.o $(OBJ_WITHOUT_MAIN)
	${CC} $(CFLAGS) $(LDFLAGS) -o $@ $< $(OBJ_WITHOUT_MAIN)

.PHONY: build-tests
build-tests: $(TEST_EXE)

#################################
# Development environment setup #
# ###############################

# main rule for setting up the dev environment with Vim
.PHONY: setup_envdev
setup_envdev: bear types

# create a compile_ommands.json file for autocompletion in Vim, requires bear
.PHONY: bear
bear: clean
	bear make all

# Make a highlight file for types.  Requires Exuberant ctags and awk
.PHONY: types
types: types.vim
types.vim: $(SRC) $(HEADERS)
	ctags --c-kinds=gstu -o- $(SRC) $(HEADERS) |\
		awk 'BEGIN{printf("syntax keyword Type\t")}\
	                {printf("%s ", $$1)}END{print ""}' > $@
