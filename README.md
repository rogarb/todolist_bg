`todolist_bg`: a file based TODO list displayed on the root of the screen
------

This program displays the content of a text file on the root windows/background
of the screen. It is aimed at being a todo file displayer but can serve any
purpose.

Usage
---
```
todolist_bg [opts] -t todofile  Display the content of todofile on the root window
todolist_bg -h                  Show this help

where [opts] can be any of the following:
	-x pos: specify the x position of the upper left part of the text
	-y pos: specify the y position of the upper left part of the text
	-d: run in background
	-f fontname: change the font
	-s fontsize: change the font size
	-c fontcolor: change the font color
	-e: extended text format, print formatted text
	-l logfile: specify the file to use for logging

Formatted text:
	*text* prints bold text
	/text/ or _text_ prints italic text
```
